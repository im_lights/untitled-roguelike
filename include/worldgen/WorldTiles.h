#ifndef WorldTiles
#define WorldTiles

#include "../dev/Profiling.h"
#include "../dev/Logging.h"

#include "../tools/Rand.h"
#include "./Geometry.h"
#include "./WorldData.h"

#include <string>
#include <vector>
#include <thread>
#include <pthread.h>
#include <utility>
#include <array>

#define WORLD_X 64
#define WORLD_Y 64

/*
 * side - edge of rectangle cut is applied to in a clockwise manner where 0 = top
 * a0 - cut line segment starting point
 * a1 - cut line segment ending point
 * b0 - point on line segment a corresponding to the 3rd point of cut triangle
 * b1 - perpedicular offset from line segment a defining the cut triangle
 */
struct Cut {
    unsigned int side;
    float a0;
    float a1;
    float b0;
    float b1;
    Cut(unsigned int _side, float _a0, float _a1, float _b0, float _b1);
};

struct Tile {
    int area;
    int zone;
    bool inbounds;
    int x;
    int y;
    TileClass tileclass;
    std::string code; // UTF-16 symbol for tile
    int mask; // color mask key
    int movement; // Movement policy for tile
    Tile();
    Tile(int _x, int _y, int a, bool in);
    const int tilecode() const;
    int zonecode();
};

struct Tilemap {
    unsigned int totalx;
    unsigned int totaly;
    std::array<std::array<Tile, WORLD_X>, WORLD_Y> tilegrid;
    Tilemap();
    Tile& at(int x, int y);
    const Tile& at(int x, int y) const;
};

// Primary Functions
std::vector<Cut> GenerateCuts(Random* r);

std::vector<Line> GenerateAreas(Random* r);

Tilemap GenerateTiles(Random* r, const std::vector<Cut>& cuts, const std::vector<Line>& areas);

// Threaded Functions
void Scramble(Random* r, Tilemap* tiles, int x, int y);

void GenTile(Tile* tile, const int& x, const int& y,
    const float& posx, const float& posy, const std::vector<Line>& areas,
    bool validRanges[], const std::vector<Cut>& cuts);

// Static Functions
static bool checkArea(const std::vector<std::pair<float, float>>& coords);

static void minmaxY(const std::vector<std::pair<float, float>>& arr, float& min, float& max);

static int findArea(const float& posx, const float& posy, const std::vector<Line>& areas, bool validRanges[]);

// TODO - move me to a test library
static int findAreaTest(const float& posx, const float& posy, const std::vector<Line>& areas, bool validRanges[]);

static bool checkCut(const std::vector<Cut>& cuts, const float& x3, const float& y3);

static bool inRangeY(const Line& line, const float& y3);

static int findNeighbors(const int& x, const int& y, const Tilemap& tiles);

static int distanceFromUnlike(const int& x, const int& y, const Tilemap& tiles, const int& lim);

static int checkUnlikeNeighbor(const int& x, const int& y, const Tilemap& tiles);

void outputWorld(const Tilemap& tiles);

#endif