#ifndef Geometry
#define Geometry

#include <cmath>
#include <utility>

struct Point {
    const float x;
    const float y;
    Point(const float _x, const float _y);
    Point();
    std::pair<float, float> value();
};

struct Line {
    const float ax;
    const float ay;
    const float bx;
    const float by;
    Line(const float _ax, const float _ay, const float _bx, const float _by);
    Line();
};

float getLength(float x1, float y1, float x2, float y2);

float getSlope(float x1, float y1, float x2, float y2);

float getAngle(float s0, float s1);

float getInterceptX(float x1, float y1, float x2, float y2, float y3);

float getInterceptY(float x1, float y1, float x2, float y2, float x3);

#endif