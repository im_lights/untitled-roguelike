#ifndef WorldChunks
#define WorldChunks

#include "../tools/Rand.h"
#include "./WorldTiles.h"

#include <array>
#include <utility>
#include <string>
#include <map>

struct Chunk : Tilemap {
    Chunk();
    Chunk(Tile tile);
    Chunk(Tilemap tiles);
};

// TODO - consider using a class here
// TODO - check if a chunk is loaded
struct Chunkmap {
    std::array<std::array<Chunk*, WORLD_X>, WORLD_Y> grid;
    std::map<std::string, Chunk*> map;
    Chunkmap();
//  Chunkmap(Tilemap tiles); If i need this, use it later
    ~Chunkmap();
    Chunk* at(int x, int y);
    const Chunk* at(int x, int y) const;
};

// Primary Functions
Chunkmap GenerateChunks(Random* r, Tilemap* tiles);

// Threaded Functions
void buildChunk(Random* r, Chunkmap* chunks, Tilemap* tiles, const int& x, const int& y);

#endif