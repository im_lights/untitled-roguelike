#ifndef WorldData
#define WorldData

#include "../dependency/rapidjson/document.h"
#include "../dependency/rapidjson/schema.h" // TODO - add schema validation for json
#include "../dependency/rapidjson/pointer.h"
#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <iostream>

enum class TileClass {
    Ground = 0, // Walkable ground tiles
    Wall = 1, // Wall tiles
    ObjectSolid = 2, // Blocking Object tiles
    ObjectEmpty = 3, // Walkable Object tiles
    Liquid = 4 // Liquid tiles
};
enum class TileGroundType {
    Grass = 0,
    ThickGrass = 1,
    Dirt = 2,
    Sand = 3,
    Snow = 4,
    Mud = 5,
    Rock = 6,
    Cobblestone = 7,
    Wood = 8,
    Tile = 9
};
enum class TileWallType {
    Wooden = 0,
    Stone = 1,
    Sandstone = 2,
    Marble = 3,
    Basalt = 4,
    Obsidian = 5,
    Cloth = 6 // For tents
};
enum class TileObjectSolid {
    Foliage = 0,
    Structure = 1, // Village Objects
    Furniture = 2, // Building interior object
    Misc = 3
};
enum class TileObjectEmpty {
    Foliage = 0,
    Structure = 1,
    Furniture = 2,
    Misc = 3
};
enum class TileLiquid {
    Water = 0,
    Ice = 1,
    Lava = 2
};

struct Tiletype {
    TileClass tileclass;
    int code;
    std::vector<int> colors;
    int weight;
    Tiletype(TileClass _t, int _code, std::vector<int> _colors, int _weight);
    Tiletype();
};

struct Tileset {
    std::vector<Tiletype> ground;
    std::vector<Tiletype> wall;
    std::vector<Tiletype> objectSolid;
    std::vector<Tiletype> objectEmpty;
    std::vector<Tiletype> liquid;
    std::vector<std::vector<Tiletype>*> sets {
        &ground,
        &wall,
        &objectSolid,
        &objectEmpty,
        &liquid
    };
    std::map<int, std::string> colors;
    std::vector<std::vector<Tiletype*>> weightmap;
};

static std::string loadFile(std::string path);
static rapidjson::Document parseJson(std::string json);
static rapidjson::Document loadTiles();
static rapidjson::Document loadColors();
static std::string generateJsonPointer(std::vector<std::string> values);

namespace Data {
    Tileset getTileset(int temp, int moisture, int tier, float terrain, float foliage);
};



#endif