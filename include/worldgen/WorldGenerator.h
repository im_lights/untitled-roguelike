#ifndef WorldGenerator
#define WorldGenerator

#include "../dev/Profiling.h"

#include "../tools/Rand.h"
#include "../tools/Utility.h"
#include "../tools/Perlin.h"
#include "./WorldTiles.h"
#include "./WorldChunks.h"
#include "./WorldData.h"
#include "./Geometry.h" // TODO - remove me

#include <vector>
#include <iostream>
#include <string>
#include <utility>
#include <algorithm>
#include <map>
#include <sstream>

static int ZONES_MIN = 3;
static int ZONES_MAX = 5;

struct Biome {
    int temp;
    int moisture;
    int tier;
    float terrain;
    float foliage;
    int area;
    int zone;
    Biome(int _temp, int _moisture, int _tier, float _terrain, float _foliage, int _a, int _z);
    Biome();
};

struct Zones {
    // Vector containing a vector of tile pointers for each zone
    std::vector<std::vector<Tile *>> tiles;
    // Vector of zone names
    std::vector<std::string> names; // TODO - zone names
    // Vector containing a vector of zone indexes for each area
    std::vector<std::vector<int>> areas;
    Zones();
};

struct World {
    std::string name;
    Zones zones;
    std::vector<Biome> biomes;
    World(std::string _name, Tilemap t, Zones z, std::vector<Biome> b);
    World();
    Tilemap tilemap;
};

std::string GenerateName(Random* r); // TODO - Add WorldNames library for name generation

std::vector<Biome> GenerateBiomes(Random* r, std::vector<std::vector<int>> z_areas);

Zones GenerateZones(Random* r, Tilemap& tiles);

World Build(Random* r, std::string name, Tilemap* tiles, Chunkmap* chunks, Zones zones, std::vector<Biome> biomes);

// Spawn World Data with given random generator
World SpawnWorld(Random* r);

#endif