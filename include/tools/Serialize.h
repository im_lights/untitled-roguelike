#ifndef Serialize
#define Serialize

#include "../dependency/rapidjson/document.h"
#include "../dependency/rapidjson/pointer.h"
#include "../dependency/rapidjson/schema.h"
#include "../dependency/rapidjson/writer.h"
#include "../worldgen/WorldGenerator.h"

#include <string>
#include <fstream>

std::string SerializeWorld(World* world);

#endif