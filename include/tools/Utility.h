#ifndef Utility
#define Utility

namespace Util {
    int limit(int min, int max, int value);
    float limit(float min, float max, float value);
    float normalize(float min, float max, float value);
};

#endif