#ifndef Rand
#define Rand

#include <ctime>
#include <cstdlib>

constexpr double fraction { 1.0 / (RAND_MAX + 1.0) };

struct Random {
    const unsigned int seed;
    
    // Initialize Random seed as current time
    Random();

    // Initialize Random seed as previous time
    Random(unsigned int val);

    // Debug Init
    Random(bool val);

    // Set srand to seed value
    void initialize();

    // Get random unsigned integer value - optionally from a range
    const int value(int min, int max);

    // Get random float value
    const float fvalue();

    // Get initial seed value
    const unsigned int getSeed();
};

#endif