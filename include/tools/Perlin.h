#ifndef Perlin
#define Perlin

#include "./Rand.h"
#include "./Utility.h"

#include <math.h>
#include <map>
#include <string>
#include <utility>
#include <vector>

static float RANGE_MIN = -1.0F;
static float RANGE_MAX = 1.0F;

// TODO - update this to be a class
struct PerlinMap {
    std::vector<std::vector<float>> data;
    PerlinMap(){}
    const float at(int x, int y) const {
        return data[y][x];
    }
};

class PerlinGenerator {
    std::map<std::string, PerlinMap*> cache;
    const float seed;
    const int rangex;
    const int rangey;
    const int totalx;
    const int totaly;
public:
    PerlinGenerator(const float _seed, const int rx, const int ry);
    PerlinMap* at(const int& x, const int& y, const bool& smooth = false);
};

static float interpolate(float a0, float a1, float w, bool smooth) {
    if (0.0 > w) return a0;
    if (1.0 < w) return a1;
    if (smooth)
        return (a1 - a0) * ((w * (w * 6.0 - 15.0) + 10.0) * w * w * w) + a0;
    else
        return (a1 - a0) * w + a0;
}

// TODO fix this bullshit
static std::pair<float, float> gradientVec(float seed, int ix, int iy) {
    float var = (seed * 200.0f);
    float random = 2920.f * sin(ix * (var + 21250.0f) + iy * 171324.f + 8912.f) * cos(ix * (var + 23000.0f) * iy * 217832.f + 9758.f);
    return std::pair<float, float>(cos(random), sin(random));
}

// Computes the dot product of the distance and gradient vectors.
static float dotGridGradient(float seed, int ix, int iy, float x, float y) {
    // Get gradient from integer coordinates
    std::pair<float, float> gradient = gradientVec(seed, ix, iy);

    // Compute the distance vector
    float dx = x - (float)ix;
    float dy = y - (float)iy;

    // Compute the dot-product
    return (dx * gradient.first + dy * gradient.second);
}

// Compute Perlin noise at coordinates x, y
static float _perlin(float seed, float x, float y, bool smooth = false) {
    // Determine grid cell coordinates
    int x0 = (int)x;
    int x1 = x0 + 1;
    int y0 = (int)y;
    int y1 = y0 + 1;

    // Determine interpolation weights
    // Could also use higher order polynomial/s-curve here
    float sx = x - (float)x0;
    float sy = y - (float)y0;

    float n0, n1, ix0, ix1, value;

    n0 = dotGridGradient(seed, x0, y0, x, y);
    n1 = dotGridGradient(seed, x1, y0, x, y);
    ix0 = interpolate(n0, n1, x, smooth);

    n0 = dotGridGradient(seed, x0, y1, x, y);
    n1 = dotGridGradient(seed, x1, y1, x, y);
    ix1 = interpolate(n0, n1, x, smooth);

    value = interpolate(ix0, ix1, y, smooth);
    return Util::limit(0.0, RANGE_MAX, Util::normalize(RANGE_MIN - 0.2F, RANGE_MAX + 0.2F, value));
}

PerlinGenerator perlin(Random* r, int rangex, int rangey);

#endif