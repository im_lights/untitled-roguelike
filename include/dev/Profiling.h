#ifndef Profiling
#define Profiling

#include "./Debug.h"
#include <chrono>
#include <string>
#include <vector>
#include <utility>
#include <iostream>

typedef std::chrono::time_point<std::chrono::system_clock> ms;

namespace Debug {
    class Profiler {
    private:
        std::vector<std::pair<std::string, ms>> ticks = {};
        bool active = false;
    public:
        bool Start(std::string tag);

        bool Tick(std::string tag);

        bool Finish();
    };
};

#endif