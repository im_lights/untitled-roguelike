#ifndef Logging
#define Logging

#include "./Debug.h"
#include <exception>
#include <deque>
#include <iostream>
#include <chrono>
#include <string>
#include <sstream>
#include <fstream>

typedef std::chrono::time_point<std::chrono::system_clock> ms;

static void writeLog(std::string str, std::string path);

namespace Debug {
    enum MessageType {
        Normal = 0,
        Warn = 1,
        Error = 2,
        Fatal = 3
    };
    void Log(std::string str, MessageType t = MessageType::Normal, bool write = false);
    class Logger {
        static std::deque<std::string> history;
        friend void Debug::Log(std::string str, MessageType t, bool write);
    };
};

#endif