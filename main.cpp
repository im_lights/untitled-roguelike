#include <stdio.h>
#include <iostream>
#include <string>

#include "./include/tools/Rand.h"
#include "./include/worldgen/WorldGenerator.h"
#include "./include/worldgen/WorldData.h"
#include "./include/tools/Serialize.h"

int main(int argc, char **argv)
{
    Random* r = new Random();
    World world = SpawnWorld(r);

    std::string json = SerializeWorld(&world);

	return 0;
}
