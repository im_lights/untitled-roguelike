#include "../include/tools/Rand.h"

Random::Random() : seed(std::time(nullptr)) {
    initialize();
}

Random::Random(unsigned int val) : seed(val) {
    initialize();
}

Random::Random(bool val) : seed(17489) {
    initialize();
}

void Random::initialize() {
    std::srand(seed);
}

const int Random::value(int min = 0, int max = RAND_MAX) {
    return min + static_cast<int>((max - min + 1) * (std::rand() * fraction));
}

const float Random::fvalue() {
    return static_cast<float>(std::rand() * fraction);
}

const unsigned int Random::getSeed() {
    return seed;
}