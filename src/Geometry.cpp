#include "../include/worldgen/Geometry.h"

#include <iostream>

Point::Point(const float _x, const float _y) : x(_x), y(_y) {}

Point::Point() : x(0.0F), y(0.0F) {}

std::pair<float, float> Point::value() {
    return std::pair<float, float> (x, y);
}

Line::Line(const float _ax, const float _ay, const float _bx, const float _by)
    : ax(_ax), ay(_ay), bx(_bx), by(_by) {}

Line::Line()
    : ax(0.0F), ay(0.0F), bx(0.0F), by(0.0F) {}

float getLength(float x1, float y1, float x2, float y2) {
    return std::sqrt(
        std::pow(std::abs(x1 - x2), 2)
      + std::pow(std::abs(y1 - y2), 2)
    );
}

float getSlope(float x1, float y1, float x2, float y2) {
    return (y2 - y1) / (x2 - x1);
}

float getAngle(float s0, float s1) {
    return std::atan(std::abs((s1 - s0) / (1 + s1 * s0)));
}

float getInterceptX(float x1, float y1, float x2, float y2, float y3) {
    return (y3 - y1) / getSlope(x1, y1, x2, y2) + x1;
}

float getInterceptY(float x1, float y1, float x2, float y2, float x3) {
    return (x3 - x1) * getSlope(x1, y1, x2, y2) + x3;
}