#include "../include/tools/Perlin.h"

PerlinGenerator::PerlinGenerator(const float _seed, const int rx, const int ry)
    : seed(_seed), rangex(rx), rangey(ry), totalx(rx * rx), totaly(ry * ry) {}

PerlinMap* PerlinGenerator::at(const int& x, const int& y, const bool& smooth) {
    // TODO check if a perlin map for a given chunk is already generated
    // If so, return it. if not, generate it and store it
    std::string key = std::to_string(x) + "," + std::to_string(y);
    PerlinMap* out;
    if (cache[key]) {
        out = cache[key];
    } else {
        out = new PerlinMap(x, y);
    }
    return out;
}

PerlinGenerator perlin(Random* r, int rangex, int rangey) {
    PerlinGenerator* out = new PerlinGenerator(r->fvalue(), rangex, rangey);

    return *out;
    /*
    std::vector<std::vector<float>> data;
    std::vector<float> row;

    float seed = r->fvalue();
    float subx = (float)rangex / 64.0f;
    float suby = (float)rangey / 64.0f;

    for (int y = 0; y < rangey; y++) {
        for (int x = 0; x < rangex; x++) {
            row.push_back(_perlin(seed, (float)x / subx, (float)y / suby, smooth));
        }
        data.push_back(row);
        row.clear();
    }
    out.data = data;
    return out;
     */
}