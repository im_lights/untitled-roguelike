#include "../include/tools/Serialize.h"

std::string SerializeWorld(World* world) {
    std::cout << "Serializing World..." << std::endl;
    Tile* t;
    std::string json;
    rapidjson::Document data;
    rapidjson::Document::AllocatorType& allocator = data.GetAllocator();
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    data.Accept(writer);
    data.SetObject();

    // Build Tiles
    rapidjson::Value columns(rapidjson::kArrayType);
    for (int y = 0; y < world->tilemap.totaly; y++) {
        rapidjson::Value row(rapidjson::kArrayType);
        for (int x = 0; x < world->tilemap.totalx; x++) {
            rapidjson::Value tile(rapidjson::kObjectType);
            t = &world->tilemap.at(x, y);

            tile.AddMember("area", t->area, allocator);
            tile.AddMember("zone", t->zone, allocator);
            tile.AddMember("inbounds", t->inbounds, allocator);
            tile.AddMember("x", t->x, allocator);
            tile.AddMember("y", t->y, allocator);
            tile.AddMember("code", *(t->code.c_str()), allocator);
            tile.AddMember("color", t->mask, allocator);
            tile.AddMember("move", t->movement, allocator);
            
            row.PushBack(tile, allocator);
        }
        columns.PushBack(row, allocator);
    }
    data.AddMember("tiles", columns, allocator);

    // Build Zones
    rapidjson::Value zones(rapidjson::kArrayType);
    for (int i = 0; i < world->biomes.size(); i++) {
        rapidjson::Value z(rapidjson::kObjectType);

        z.AddMember("name", *(world->zones.names[world->biomes[i].zone].c_str()), allocator);
        z.AddMember("area", world->biomes[i].area, allocator);
        z.AddMember("tier", world->biomes[i].tier, allocator);

        zones.PushBack(z, allocator);
    }
    data.AddMember("zones", zones, allocator);

    // Load World Schema
    std::string line, schemajson;
    std::fstream file("./data/schema/world.schema", std::fstream::in);
    if (file.is_open()) {
        while(std::getline(file, line))
            schemajson += line;
    }
    file.close();

    rapidjson::Document schemadoc;
    rapidjson::StringBuffer buffer2;
    if (!schemadoc.Parse(schemajson.c_str()).HasParseError()) {
        rapidjson::SchemaDocument schema(schemadoc);
        rapidjson::SchemaValidator validator(schema);
        if (!data.Accept(validator)) {
            validator.GetInvalidSchemaPointer().StringifyUriFragment(buffer2);
            std::cerr << "Invalid Schema: " << buffer2.GetString() << std::endl;
            std::cerr << "Invalid Keyword: " << validator.GetInvalidSchemaKeyword() << std::endl;
            buffer.Clear();
            validator.GetInvalidDocumentPointer().StringifyUriFragment(buffer2);
            std::cerr << "Invalid Json Document: " << buffer2.GetString() << std::endl;
            return std::string("{}");
        }
    }

    json = buffer.GetString();

    std::cout << json << std::endl;

    return json;
}