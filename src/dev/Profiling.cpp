#include "../../include/dev/Profiling.h"

bool Debug::Profiler::Start(std::string tag) {
    if (ticks.size() != 0 || active) {
        return false; // Can't run concurrent profilers
    }
    ticks.push_back(std::pair<std::string, ms>(tag, std::chrono::system_clock::now()));
    active = true;
    return active;
}

bool Debug::Profiler::Tick(std::string tag) {
    if (ticks.size() == 0 || !active) {
        return false;
    }
    ticks.push_back(std::pair<std::string, ms>(tag, std::chrono::system_clock::now()));
    return active;
}

bool Debug::Profiler::Finish() {
    if (ticks.size() == 0 || !active) {
        return false;
    }

    std::chrono::duration<double> elapsed;
    std::cout << "Profiler Report:" << std::endl;
    for (int i = 0; i < ticks.size(); i++) {
        std::cout << "[" << i << "] " << ticks[i].first << std::endl;
        std::cout << "[" << std::chrono::system_clock::to_time_t(ticks[i].second) << "] ";
        if (i != 0) {
            elapsed = ticks[i].second - ticks[i - 1].second;
            std::cout << elapsed.count() << " since last tick." << std::endl;
        }
        std::cout << "--------" << std::endl;
    }
    std::cout << "End of Report." << std::endl;

    ticks.clear();
    active = false;
    return !active;
}