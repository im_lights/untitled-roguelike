#include "../../include/dev/Logging.h"

std::deque<std::string> Debug::Logger::history {};

void writeLog(std::string str, std::string path) {
    std::ofstream file(path.c_str());
    if (file.is_open()) {
        file << str;
    } else {
        std::cerr << "Unable to write to log file." << std::endl;
    }
    file.close();
}

void Debug::Log(std::string str, MessageType t, bool write) {
    ms now = std::chrono::system_clock::now();
    std::string path, color;
    std::stringstream out;
    bool kill = false, err;

    switch(t) {
        default:
        case MessageType::Normal:
            color = "\x1b[0m";
            path = "./logs/debug.log";
            err = false;
        break;
        case MessageType::Warn:
            color = "\x1b[33m";
            path = "./logs/error.log";
            err = true;
        break;
        case MessageType::Fatal:
            kill = true;
        case MessageType::Error:
            color = "\x1b[31m";
            path = "./logs/error.log";
            err = true;
        break;
    }

    out << (kill ? "(\x1b[31mFATAL\x1b[0m) " : "")
        << "[" << std::chrono::system_clock::to_time_t(now) << "] "
        << color << str << std::endl;
    Debug::Logger::history.push_back(out.str());

    if (err)
        std::cerr << out.str();
    else
        std::cout << out.str();

    if (write)
        writeLog(out.str(), path);

    if (kill)
        std::terminate();
}