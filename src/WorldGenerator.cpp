#include "../include/worldgen/WorldGenerator.h"

Biome::Biome() {}
Biome::Biome(int _temp, int _moisture, int _tier, float _terrain, float _foliage, int _a, int _z)
    : temp(_temp), moisture(_moisture), tier(_tier), terrain(_terrain), foliage(_foliage),
        area(_a), zone(_z) {}

World::World(std::string _name, Tilemap t, Zones z, std::vector<Biome> b)
    : name(_name), tilemap(t), zones(z), biomes(b) {}

World::World() {} // Placeholder TODO - Remove

Zones::Zones(){}

std::string GenerateName(Random* r) {
    // TODO - handle name generation
    return "Untitled World";
}

std::vector<Biome> GenerateBiomes(Random* r, std::vector<std::vector<int>> z_areas) {
    std::vector<Biome> output;
    bool availableTiers[5] { true, true, true, true, true };
    int tier, temp, moisture;
    float terrain, foliage;

    for (int a = 0; a < 5; a++) {
        // Generate Area-base biome values
        if (a == 0) {
            // Always tier 1 with no extreme values
            temp = 2;
            moisture = 2;
            tier = 1;
        } else {
            temp = r->value(0, 4);
            moisture = r->value(0, 4);
            do {
                tier = r->value(2, 5);
            } while (!availableTiers[tier - 1]);
        }
        availableTiers[tier - 1] = false;

        // Generate Biomes per-zone with minor variations between zones
        Biome biome;
        for (int i = 0; i < z_areas[a].size(); i++) {
            biome = {
                Util::limit(0, 4, r->value(temp - 1, temp + 1)), // Temp
                Util::limit(0, 4, r->value(moisture - 1, moisture + 1)), // Moisure
                tier,
                r->fvalue() * 0.3F + 0.6F, // Terrain
                (r->fvalue() * ((moisture < 2 || temp > 3 || temp < 2) ? 0.5F : 1.0F)) * 0.3F + 0.6F, // Foliage
                a, // Area
                z_areas[a][i] // Zone
            };
            output.push_back(biome);
        }
    }
    return output;
}

Zones GenerateZones(Random* r, Tilemap& tiles) {
    Zones zones = Zones();

    // Seperate tile pointers into areas
    std::vector<std::vector<Tile*>> areas;
    for (int i = 0; i < 5; i++) {
        areas.push_back(std::vector<Tile*>());
    }
    Tile* t;
    for (int y = 0; y < tiles.totaly; y++) {
        for (int x = 0; x < tiles.totalx; x++) {
            t = &tiles.at(x, y);
            areas[t->area - 1].push_back(t);
        }
    }

    // Generate zone seeds for each area
    std::vector<std::vector<Tile *>> z_tiles; // vector of zones
    std::vector<std::vector<int>> z_areas; // vector of area subzones
    std::vector<int> c_zones;
    std::vector<Tile *> z;
    int n;
    int selected[ZONES_MAX];
    for (int a = 0; a < 5; a++) {
        n = r->value(ZONES_MIN, ZONES_MAX);
        c_zones.clear();
        for (int i = 0; i < n; i++) {
            do {
                selected[i] = r->value(0, areas[a].size() - 1);
            } while(
                (i - 1 >= 0 && selected[i - 1] == selected[i]) ||
                (i - 2 >= 0 && selected[i - 2] == selected[i])
            );
            // create seed zone tile
            z = { areas[a][selected[i]] };
            z.reserve(areas[a].size());
            z_tiles.push_back(z);

            // add this zone to the area's zone vector
            c_zones.push_back(z_tiles.size() - 1);
        }
        z_areas.push_back(c_zones);
    }

    // Iterate through tiles in area and add them to the zone of the nearest seed
    float dist[3] { 0.0, 0.0, 0.0 };
    int zptr, lowest;
    for (int a = 0; a < 5; a++) {
        for (Tile* tile : areas[a]) {
            for (int i = 0; i < z_areas[a].size(); i++) {
                zptr = z_areas[a][i];
                dist[i] = getLength(tile->x, tile->y, z_tiles[zptr][0]->x, z_tiles[zptr][0]->y);
            }
            lowest = 0;
            for (int i = 0; i < z_areas[a].size(); i++) {
                if (dist[i] < dist[lowest])
                    lowest = i;
            }
            zptr = z_areas[a][lowest];
            tile->zone = zptr;
            z_tiles[zptr].push_back(tile);
        }
    }

    zones.tiles = z_tiles;
    zones.areas = z_areas;

    return zones;
}

World Build(Random* r, std::string name, Tilemap* tiles, Chunkmap* chunks, Zones zones, std::vector<Biome> biomes) {
    World world;
    Biome* biome;
    Chunk* c;
    Tile* t;
    PerlinMap terrain = perlin(r, WORLD_X, WORLD_Y, true);
    PerlinMap foliage = perlin(r, WORLD_X, WORLD_Y, false);
    PerlinMap structures = perlin(r, WORLD_X, WORLD_Y, false);

    // Generate large world structure by chunk
    // TODO - run this loop multiple times for different terrain features- mountains, rivers, lakes
    for (int y = 0; y < chunks->grid.size(); y++) {
        for (int x = 0; x < chunks->grid.size(); x++) {
            c = chunks->at(x, y); // Chunk currently being looked at
            t = &tiles->at(x, y); // Properties and zoning this chunk will inherit
            biome = &biomes[zones.areas[t->area][t->zone]]; // Biome Data for chunk

            // Use perlin terrain map to decide if this chunk needs a terrain feature
            // Look for surrounding tiles that also peak above a similar threshold and
            // include them in the terrain generation- they will then be flagged to be skipped


            // Determine base tile class
            if (biome.terrain <= terrain.at(x, y)) {
                t->tileclass = TileClass::Wall;
                t->movement = false;
            } else if (
                biome.terrain
                / (2.0 - (float)(Util::limit(0, 4, biome.moisture - 2)) * 0.2)
                >= terrain.at(x, y)
            ) {
                t->tileclass = TileClass::Liquid;
                t->movement = true;
            } else if (biome.foliage <= foliage.at(x, y)) {
                t->tileclass = TileClass::ObjectSolid;
                t->movement = false;
            } else {
                t->tileclass = TileClass::Ground;
                t->movement = true;
            }
        }
    }

    // Generate Tiles for each biome
    int tier;
    Tileset tileset;
    Tiletype type;
    for (int a = 0; a < 5; a++) {
        for (int i = 0; i < zones.areas[a].size(); i++) {
            biome = biomes[zones.areas[a][i]];
            tier = biome.tier;

            // Generate symbol and color sets per biome
            tileset = Data::getTileset(biome.temp, biome.moisture, biome.tier, biome.terrain, biome.foliage);

            // Assign symbols and colors to tiles
            for (Tile* t : zones.tiles[zones.areas[a][i]]) {
                type = *tileset.weightmap[
                    static_cast<int>(t->tileclass) // Tileclass
                ][
                    r->value(0, tileset.weightmap[static_cast<int>(t->tileclass)].size() - 1) // random index
                ];
                t->code = type.code;
                t->mask = type.colors[r->value(0, type.colors.size() - 1)]; // Random color value
            }

            // Generate Zone-specific structures
        }

        // Generate Area-specific structures
    }
    world = World(name, tiles, zones, biomes);
    return world;
}

World SpawnWorld(Random* r) {
    std::cout << "Generating World..." << std::endl;

    //Debug::Profiler::Start("Begin Generation");
    // Generate world name
    std::string name = GenerateName(r);

    //Debug::Profiler::Tick("Name Generated");
    // Generate border cuts
    std::vector<Cut> cuts = GenerateCuts(r);

    //Debug::Profiler::Tick("Cuts Generated");
    // Generate division points
    std::vector<Line> areas = GenerateAreas(r);

    //Debug::Profiler::Tick("Areas Generated");
    // Generate base tilemap
    Tilemap tiles = GenerateTiles(r, cuts, areas);
    std::cout << "Tiles Generated" << std::endl;

    //Debug::Profiler::Tick("Tilemap Built");
    //Debug::Profiler::Finish();
    // Generate Zones
    Zones zones = GenerateZones(r, tiles);
    std::cout << "Zones Generated" << std::endl;

    //Debug::Profiler::Tick("Zones Generated");
    // Generate and roll biomes
    std::vector<Biome> biomes = GenerateBiomes(r, zones.areas);
    std::cout << "Biomes Generated" << std::endl;

    ///Debug::Profiler::Tick("Biomes Generated");

    Chunkmap chunks = GenerateChunks(r, &tiles);
    // Build Terrain features and structures

    World w = Build(r, name, &tiles, &chunks, zones, biomes);
    //Debug::Profiler::Tick("World Built");

    //Debug::Profiler::Finish();
    return w;
}