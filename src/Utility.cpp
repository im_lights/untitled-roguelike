#include "../include/tools/Utility.h"

int Util::limit(int min, int max, int value) {
    int out = value;
    if (max < value)
        out = max;
    if (min > value)
        out = min;
    return out;
}

float Util::limit(float min, float max, float value) {
    float out = value;
    if (max < value)
        out = max;
    if (min > value)
        out = min;
    return out;
}

float Util::normalize(float min, float max, float value) {
    return (value - min) / (max - min);
}