#include "../include/worldgen/WorldChunks.h"

Chunk::Chunk() : Tilemap() {}

Chunk::Chunk(Tile tile) : Tilemap() {
    tile.tileclass = TileClass::Ground; // Give all default tiles a ground class
    for (int y = 0; y < totaly; y++) {
        tilegrid[y].fill(tile);
    }
}

Chunk::Chunk(Tilemap tiles) : Tilemap(tiles) {}

Chunkmap::Chunkmap() {}

Chunkmap::~Chunkmap() {} // TODO - deallocate chunks individually

Chunkmap GenerateChunks(Random* r, Tilemap* tiles) {
    Chunkmap* chunks = new Chunkmap(); // Be sure to deallocate this memory after writing world data

    // Generate Blended chunkmap representing the tiles Tilemap
    int ix, iy;
    for (int y = 0; y < tiles->totaly; y++) {
        for (int x = 0; x < tiles->totalx; x++) {
            ix = tiles->totalx - x - 1;
            iy = tiles->totaly - y - 1;

            buildChunk(r, chunks, tiles, x, y);
            buildChunk(r, chunks, tiles, ix, y);
            buildChunk(r, chunks, tiles, x, iy);
            buildChunk(r, chunks, tiles, ix, iy);
        }
    }

    return *chunks;
}

// Threaded Functions
void buildChunk(Random* r, Chunkmap* chunks, Tilemap* tiles, const int& x, const int& y) {

    // Create chunk in chunkmap and put pointer in array
    std::string key(std::to_string(x) + "," + std::to_string(y));
    Chunk* chunk = nullptr;
    chunks->map.insert(std::pair<std::string, Chunk*> {
        key,
        new Chunk(tiles->at(x, y)) // Deallocated in chunkmap destructor
    });
    chunk = chunks->map[key];
    chunks->grid[y][x] = chunk;

    // Check neighboring tiles in tilegrid and apply some basic blending
    bool neighbors[8] { false, false, false, false, false, false, false, false };
    Tile* tileptrs[8];
    int i = 0;
    Tile* t, * center = &tiles->at(x, y);
    for (int y0 = y - 1; y0 < y + 2; y0++) {
        for (int x0 = x - 1; x0 < x + 2; x0++) {
            if (x0 == x && y0 == y) continue; // skip same tile without incrementing i
            if (x0 >= 0 && y0 >= 0 && x0 < tiles->totalx && y0 < tiles->totaly) {
                t = &tiles->at(x0, y0);
                tileptrs[i] = t;
                if (t->area != center->area || t->zone != center->zone)
                    neighbors[i] = true;
            }
            i++;
        }
    }

    // Restructure array so index can be used to determine direction
    bool dirs[8] {
        &neighbors[0], &neighbors[1], &neighbors[2],
        &neighbors[4], &neighbors[7], &neighbors[6],
        &neighbors[5], &neighbors[3]
    };
    int ptrindex[8] { 0, 1, 2, 4, 7, 6, 5, 3 };
    int xflag, yflag, mflag, statemap[8][2] {
        { 1, 1 }, // NW
        { 0, 1 }, // N
        { 2, 1 }, // NE
        { 2, 0 }, // E
        { 2, 2 }, // SE
        { 0, 2 }, // S
        { 1, 2 }, // SW
        { 1, 0 }  // W
    }, range[3][2] {
        { 8, WORLD_X - 8 }, // Neutral Range
        { 0, 8 }, // Min Range
        { WORLD_X - 8, WORLD_X }, // Max Range
    };
    // Check each direction starting with Northwest
    for (i = 0; i < 8; i++) {
        if (!dirs[i]) continue;

        // Apply blend operations based on stateflag
        xflag = statemap[i][0];
        yflag = statemap[i][1];
        for (int yptr = range[yflag][0]; yptr < range[yflag][1]; yptr++) {
            for (int xptr = range[xflag][0]; xptr < range[xflag][1]; xptr++) {
                // Mirror the primary axis of blending
                mflag = ((yflag == 0 ^ xflag == 0) ? ((xflag > 0) ? xflag : yflag) : xflag);
                // Determine if we flip this tile to the neighboring area
                if ((mflag % 2 == 0) ^ (r->fvalue() > 0.5F * (1.0 - range[mflag][0] / range[mflag][1]))) {
                    chunk->at(xptr, yptr).area = tileptrs[ptrindex[i]]->area;
                    chunk->at(xptr, yptr).zone = tileptrs[ptrindex[i]]->zone;
                }
            }
        }
    }
}