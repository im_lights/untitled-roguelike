#include "../include/worldgen/WorldTiles.h"

// TODO - re-map the correct opcodes
std::vector<std::vector<std::pair<int, int>>> findAreaOps {
    {
        { 0, 3 },
        { 1, 0 },
        { 2, 2 },
        { 3, 1 }
    },
    {
        { 4, 1 },
        { 0, 2 },
        { 5, 2 }
    },
    {
        { 5, 3 },
        { 1, 1 },
        { 6, 1 }
    },
    {
        { 6, 0 },
        { 2, 3 },
        { 7, 3 }
    },
    {
        { 7, 2 },
        { 3, 0 },
        { 4, 0 }
    }
};

// Cut
Cut::Cut(unsigned int _side, float _a0, float _a1, float _b0, float _b1) {
    side = _side;
    a0 = _a0;
    a1 = _a1;
    b0 = _b0;
    b1 = _b1;
}

// Tile
Tile::Tile() {}
Tile::Tile(int _x, int _y, int a, bool in) : x(_x), y(_y), area(a), inbounds(in), zone(-1) {
    // Run Assertions
    assert(a > 0);
    assert(a < 6);
}

const int Tile::tilecode() const {
    int out = 0;
    if (inbounds)
        out = (zone > -1) ? zone : area;
    return out;
}

// Tilemap
Tilemap::Tilemap() : totalx(WORLD_X), totaly(WORLD_Y) {
    tilegrid.fill({});
}

Tile& Tilemap::at(int x, int y) {
    return tilegrid[y][x];
}

const Tile& Tilemap::at(int x, int y) const {
    return tilegrid[y][x];
}

// Main Functions
std::vector<Cut> GenerateCuts(Random* r) {
    std::vector<Cut> cuts;

    // Generate between 2 and 6 cuts
    int side;
    int previous[4] {0, 0, 0, 0};
    int a0, a1;
    for (unsigned int i = 0; i < r->value(2, 6); i++) {
        do {
            side = r->value(0, 3);
        } while(previous[side] >= 2);
        a0 = r->fvalue();
        a1 = r->fvalue();
        cuts.push_back(Cut(
            side, // Side
            a0, // a0
            a1, // a1
            r->fvalue() * (a1 - a0) + a0, // b0
            r->fvalue() * 0.2  // b1
        ));
        previous[side]++;
    }

    return cuts;
}

std::vector<Line> GenerateAreas(Random* r) {
    std::vector<Line> output;

    // Generate an interior quad that has an acceptable area
    std::vector<std::pair<float, float>> coords;
    do {
        coords.clear();
        coords.push_back(std::pair<float, float>(r->fvalue() * 0.5 + 0.1, r->fvalue() * 0.5 + 0.1));
        coords.push_back(std::pair<float, float>(0.9 - r->fvalue() * 0.5, r->fvalue() * 0.5 + 0.1));
        coords.push_back(std::pair<float, float>(0.9 - r->fvalue() * 0.5, 0.9 - r->fvalue() * 0.5));
        coords.push_back(std::pair<float, float>(r->fvalue() * 0.5 + 0.1, 0.9 - r->fvalue() * 0.5));
    } while (!checkArea(coords));
    Point points[4] {
        Point(coords[0].first, coords[0].second),
        Point(coords[1].first, coords[1].second),
        Point(coords[2].first, coords[2].second),
        Point(coords[3].first, coords[3].second)
    };

    for (unsigned int i = 0; i < 4; i++) {
        output.push_back(Line(
            points[i].value().first,
            points[i].value().second,
            points[(i + 1) % 4].value().first,
            points[(i + 1) % 4].value().second
        ));
    }

    // Generate exterior points
    Point p2[4] {
        Point(r->fvalue() * 0.1, 0.0),
        Point(1.0, r->fvalue() * 0.1),
        Point(1.0 - r->fvalue() * 0.1, 1.0),
        Point(0.0, 1.0 - r->fvalue() * 0.1)
    };

    // Generate closing lines
    for (int i = 0; i < 4; i++) {
        output.push_back(
            Line(
                p2[i].value().first,
                p2[i].value().second,
                points[i].value().first,
                points[i].value().second
            )
        );
    }

    return output;
}

Tilemap GenerateTiles(Random* r, const std::vector<Cut>& cuts, const std::vector<Line>& areas) {
    float interval = 1.0 / static_cast<float>(WORLD_X);
    Tilemap tiles = Tilemap();

    // Generate Area Ranges
    std::vector<std::pair<float, float>> ranges;
    std::vector<std::pair<float, float>> arr {
        std::pair<float, float> {areas[0].ax, areas[0].ay},
        std::pair<float, float> {areas[1].ax, areas[1].ay},
        std::pair<float, float> {areas[2].ax, areas[2].ay},
        std::pair<float, float> {areas[3].ax, areas[3].ay}
    };
    float min, max;
    minmaxY(arr, min, max);

    ranges.push_back(std::pair<float, float>(min, max)); // Area 0
    for (int i = 0; i < 4; i++) {
        arr.clear();
        arr = {
            std::pair<float, float> {areas[i + 4].ax, areas[i + 4].ay},
            std::pair<float, float> {areas[i].ax, areas[i].ay},
            std::pair<float, float> {areas[(i + 1) % 4].ax, areas[(i + 1) % 4].ay},
            std::pair<float, float> {areas[((i + 1) % 4) + 4].ax, areas[((i + 1) % 4) + 4].ay}
        };
        minmaxY(arr, min, max);
        ranges.push_back(std::pair<float, float>(min, max));
    }

    std::thread t0, t1, t2, t3;
    int ix, iy;
    float posx, posy;
    bool validRanges[5] = {};
    bool validRangesi[5] = {};

    // Assign areas to tiles
    for (int y = 0; y < tiles.totaly / 2; y++) {
        posy = static_cast<float>(y) * interval;

        // Check valid ranges of this y value
        for (int i = 0; i < 5; i++) {
            validRanges[i] = posy >= ranges[i].first && posy <= ranges[i].second;
            validRangesi[i] = 1.0 - posy >= ranges[i].first && 1.0 - posy <= ranges[i].second;
        }

        // calculate the range of X values at this Y which put it in that area
        for (int x = 0; x < tiles.totalx / 2; x++) {
            posx = static_cast<float>(x) * interval;
            ix = tiles.totalx - x - 1;
            iy = tiles.totaly - y - 1;

            t0 = std::thread(GenTile, &tiles.tilegrid[y][x], x, y, posx, posy, areas, validRanges, cuts);
            t1 = std::thread(GenTile, &tiles.tilegrid[y][ix], ix, y, 1.0 - posx, posy, areas, validRanges, cuts);
            t2 = std::thread(GenTile, &tiles.tilegrid[iy][x], x, iy, posx, 1.0 - posy, areas, validRangesi, cuts);
            t3 = std::thread(GenTile, &tiles.tilegrid[iy][ix], ix, iy, 1.0 - posx, 1.0 - posy, areas, validRangesi, cuts);
            t0.join();
            t1.join();
            t2.join();
            t3.join();
        }
    }

    // Randomize outer tiles
    for (int i = 0; i < WORLD_X / 2; i++) {
        ix = WORLD_X - 1 - i;
        iy = WORLD_Y - 1 - i;
        // west
        tiles.at(0, i).inbounds = r->fvalue() > 0.4;
        tiles.at(0, iy).inbounds = r->fvalue() > 0.4;
        tiles.at(1, i).inbounds = r->fvalue() > 0.25;
        tiles.at(1, iy).inbounds = r->fvalue() > 0.25;
        // north
        tiles.at(i, 0).inbounds = r->fvalue() > 0.4;
        tiles.at(ix, 0).inbounds = r->fvalue() > 0.4;
        tiles.at(i, 1).inbounds = r->fvalue() > 0.25;
        tiles.at(ix, 1).inbounds = r->fvalue() > 0.25;
        // south
        tiles.at(i, WORLD_Y - 1).inbounds = r->fvalue() > 0.4;
        tiles.at(ix, WORLD_Y - 1).inbounds = r->fvalue() > 0.4;
        tiles.at(i, WORLD_Y - 2).inbounds = r->fvalue() > 0.25;
        tiles.at(ix, WORLD_Y - 2).inbounds = r->fvalue() > 0.25;
        // east
        tiles.at(WORLD_X - 1, i).inbounds = r->fvalue() > 0.4;
        tiles.at(WORLD_X - 1, iy).inbounds = r->fvalue() > 0.4;
        tiles.at(WORLD_X - 2, i).inbounds = r->fvalue() > 0.25;
        tiles.at(WORLD_X - 2, iy).inbounds = r->fvalue() > 0.25;
    }
    
    // Randomize Area borders
    for (int i = 1; i < 3; i++) {
        for (int y = 0; y < tiles.totaly / 2; y++) {
            for (int x = 0; x < tiles.totalx / 2; x++) {
                t0 = std::thread(Scramble, r, &tiles, x, y);
                t1 = std::thread(Scramble, r, &tiles, tiles.totalx - x - 1, y);
                t2 = std::thread(Scramble, r, &tiles, x, tiles.totaly - y - 1);
                t3 = std::thread(Scramble, r, &tiles, tiles.totalx - x - 1, tiles.totaly - y - 1);
                t0.join();
                t1.join();
                t2.join();
                t3.join();
            }
        }
    }

    outputWorld(tiles);

    return tiles;
}

// Threaded Functions
void Scramble(Random* r, Tilemap* tiles, int x, int y) {
    int neighbor = checkUnlikeNeighbor(x, y, *tiles);
    if (neighbor == 0) return;

    int n2 = findNeighbors(x, y, *tiles);
    float rand = r->fvalue();

    // like < 3 AND rand < 1.8 / like + 1 OR rand < (1.5 / dist) / like
    // TODO - try out a simpler conditional
    if (
        (n2 < 3 && rand < 1.8F / static_cast<float>(n2) + 1.0F) ||
        (rand < (1.5F / static_cast<float>(
            distanceFromUnlike(x, y, *tiles, 2)
        )) / static_cast<float>(n2))
    ) {
        tiles->at(x, y).area = neighbor;
    }
}

void GenTile(Tile* tile, const int& x, const int& y,
    const float& posx, const float& posy, const std::vector<Line>& areas,
    bool validRanges[], const std::vector<Cut>& cuts) {
    
    *tile = Tile(
        x,
        y,
        findArea(posx, posy, areas, validRanges),
        checkCut(cuts, posx, posy)
    );
}


// Static Functions
bool checkArea(const std::vector<std::pair<float, float>>& coords) {
    // Check length and slope of each side
    float lengths[4], slopes[4];
    int a, b;
    for (unsigned int i = 0; i < 4; i++) {
        a = i % 4;
        b = (i + 1) % 4;
        lengths[i] = getLength(coords[a].first, coords[a].second, coords[b].first, coords[b].second);
        slopes[i] = getSlope(coords[a].first, coords[a].second, coords[b].first, coords[b].second);
    }

    // Check angles of lines
    float angle0 = getAngle(slopes[0], slopes[3]);
    float angle1 = getAngle(slopes[1], slopes[2]);

    // Do some triggy stuff idk
    float out = (0.5 * lengths[0] * lengths[3] * std::sin(angle0)) + (0.5 * lengths[1] * lengths[2] * std::sin(angle1));
    return 0.35 > out && out > 0.25;
}

void minmaxY(const std::vector<std::pair<float, float>>& points, float& min, float& max) {
    min = 1.0;
    max = 0.0;
    float current;

    for (int i = 0; i < points.size(); i++) {
        current = points[i].second;
        if (min > current)
            min = current;
        if (max < current)
            max = current;
    }
}

// 00 (0) = greater than, no slope, 01 (1) = less than, no slope,
//10 (2) = greater than, negative slope, 11 (3) = less than, positive slope
int findArea(const float& posx, const float& posy, const std::vector<Line>& areas, bool validRanges[]) {
    int out;
    bool pass;
    float x1, y1, x2, y2;
    for (int i = 0; i < 5; i++) {
        if (!validRanges[i]) continue;
        pass = true;
        for (int n = 0; n < findAreaOps[i].size(); n++) {
            if (inRangeY(areas[findAreaOps[i][n].first], posy)) {
                x1 = areas[findAreaOps[i][n].first].ax;
                y1 = areas[findAreaOps[i][n].first].ay;
                x2 = areas[findAreaOps[i][n].first].bx;
                y2 = areas[findAreaOps[i][n].first].by;
                pass &= ((findAreaOps[i][n].second > 1) ? (getSlope(x1, y1, x2, y2) > 0.0) : false)
                    ^ ((findAreaOps[i][n].second % 2 == 1) ^ (getInterceptX(x1, y1, x2, y2, posy) > posx));
            }
        }
        if (pass) {
            out = i + 1;
            break;
        }
    }
assert(out > 0 && out < 6);
assert(out == findAreaTest(posx, posy, areas, validRanges));
    return out;
}

int findAreaTest(const float& posx, const float& posy, const std::vector<Line>& areas, bool validRanges[]) {
    int out;
    bool pass;
    float x1, y1, x2, y2;
    for (int i = 0; i < 5; i++) {
        if (!validRanges[i]) continue;
        pass = true;

        switch(i) {
            case 0: // 0,1,2,3
            {
                if (inRangeY(areas[0], posy)) {
                    x1 = areas[0].ax;
                    y1 = areas[0].ay;
                    x2 = areas[0].bx;
                    y2 = areas[0].by;
                    pass &= (getSlope(x1, y1, x2, y2) < 0.0) ^ (getInterceptX(x1, y1, x2, y2, posy) > posx);
                }
                if (inRangeY(areas[1], posy)) {
                    x1 = areas[1].ax;
                    y1 = areas[1].ay;
                    x2 = areas[1].bx;
                    y2 = areas[1].by;
                    pass &= (getInterceptX(x1, y1, x2, y2, posy) > posx);
                }
                if (inRangeY(areas[2], posy)) {
                    x1 = areas[2].ax;
                    y1 = areas[2].ay;
                    x2 = areas[2].bx;
                    y2 = areas[2].by;
                    pass &= (getSlope(x1, y1, x2, y2) < 0.0) ^ (getInterceptX(x1, y1, x2, y2, posy) < posx);
                }
                if (inRangeY(areas[3], posy)) {
                    x1 = areas[3].ax;
                    y1 = areas[3].ay;
                    x2 = areas[3].bx;
                    y2 = areas[3].by;
                    pass &= (getInterceptX(x1, y1, x2, y2, posy) < posx);
                }
            } break;
            case 1: // 4,0,5
            {
                if (inRangeY(areas[4], posy)) {
                    x1 = areas[4].ax;
                    y1 = areas[4].ay;
                    x2 = areas[4].bx;
                    y2 = areas[4].by;
                    pass &= (getInterceptX(x1, y1, x2, y2, posy) < posx);
                }
                if (inRangeY(areas[0], posy)) {
                    x1 = areas[0].ax;
                    y1 = areas[0].ay;
                    x2 = areas[0].bx;
                    y2 = areas[0].by;
                    pass &= (getSlope(x1, y1, x2, y2) < 0.0) ^ (getInterceptX(x1, y1, x2, y2, posy) < posx);
                }
                if (inRangeY(areas[5], posy)) {
                    x1 = areas[5].ax;
                    y1 = areas[5].ay;
                    x2 = areas[5].bx;
                    y2 = areas[5].by;
                    pass &= (getSlope(x1, y1, x2, y2) < 0.0) ^ (getInterceptX(x1, y1, x2, y2, posy) < posx);
                }
            } break;
            case 2: // 5,1,6
            {
                if (inRangeY(areas[5], posy)) {
                    x1 = areas[5].ax;
                    y1 = areas[5].ay;
                    x2 = areas[5].bx;
                    y2 = areas[5].by;
                    pass &= (getSlope(x1, y1, x2, y2) < 0.0) ^ (getInterceptX(x1, y1, x2, y2, posy) > posx);
                }
                if (inRangeY(areas[1], posy)) {
                    x1 = areas[1].ax;
                    y1 = areas[1].ay;
                    x2 = areas[1].bx;
                    y2 = areas[1].by;
                    pass &= (getInterceptX(x1, y1, x2, y2, posy) < posx);
                }
                if (inRangeY(areas[6], posy)) {
                    x1 = areas[6].ax;
                    y1 = areas[6].ay;
                    x2 = areas[6].bx;
                    y2 = areas[6].by;
                    pass &= (getInterceptX(x1, y1, x2, y2, posy) < posx);
                }
            } break;
            case 3: // 6,2,7
            {
                if (inRangeY(areas[6], posy)) {
                    x1 = areas[6].ax;
                    y1 = areas[6].ay;
                    x2 = areas[6].bx;
                    y2 = areas[6].by;
                    pass &= (getInterceptX(x1, y1, x2, y2, posy) > posx);
                }
                if (inRangeY(areas[2], posy)) {
                    x1 = areas[2].ax;
                    y1 = areas[2].ay;
                    x2 = areas[2].bx;
                    y2 = areas[2].by;
                    pass &= (getSlope(x1, y1, x2, y2) < 0.0) ^ (getInterceptX(x1, y1, x2, y2, posy) > posx);
                }
                if (inRangeY(areas[7], posy)) {
                    x1 = areas[7].ax;
                    y1 = areas[7].ay;
                    x2 = areas[7].bx;
                    y2 = areas[7].by;
                    pass &= (getSlope(x1, y1, x2, y2) < 0.0) ^ (getInterceptX(x1, y1, x2, y2, posy) > posx);
                }
            } break;
            case 4: // 7,3,4
            {
                if (inRangeY(areas[7], posy)) {
                    x1 = areas[7].ax;
                    y1 = areas[7].ay;
                    x2 = areas[7].bx;
                    y2 = areas[7].by;
                    pass &= (getSlope(x1, y1, x2, y2) < 0.0) ^ (getInterceptX(x1, y1, x2, y2, posy) < posx);
                }
                if (inRangeY(areas[3], posy)) {
                    x1 = areas[3].ax;
                    y1 = areas[3].ay;
                    x2 = areas[3].bx;
                    y2 = areas[3].by;
                    pass &= (getInterceptX(x1, y1, x2, y2, posy) > posx);
                }
                if (inRangeY(areas[4], posy)) {
                    x1 = areas[4].ax;
                    y1 = areas[4].ay;
                    x2 = areas[4].bx;
                    y2 = areas[4].by;
                    pass &= (getInterceptX(x1, y1, x2, y2, posy) > posx);
                }
            } break;
        }

        if (pass) {
            out = i + 1;
            break;
        }
    }
    assert(out > 0 && out < 6);
    return out;
}

bool checkCut(const std::vector<Cut>& cuts, const float& x3, const float& y3) {
    bool out = false;
    float a0, a1, b0, b1;
    for (int i = 0; i < cuts.size(); i++) {
        a0 = cuts[i].a0;
        a1 = cuts[i].a1;
        b0 = cuts[i].b0;
        b1 = cuts[i].b1;
        switch(cuts[i].side) {
            case 0:
                if (y3 > b1) break;
                out = getInterceptX(a0, 0.0, b0, b1, y3) < x3
                    && getInterceptX(a1, 0.0, b0, b1, y3) > x3;
            break;
            case 1:
                if (x3 < 1.0 - b1) break;
                out = getInterceptY(1.0, a0, 1.0 - b1, b0, x3) > y3
                    && getInterceptY(1.0, a1, 1.0 - b1, b0, x3) < y3;
            break;
            case 2:
                if (y3 < 1.0 - b1) break;
                out = getInterceptX(1.0 - a0, 1.0, 1.0 - b0, 1.0 - b1, y3) > x3
                    && getInterceptX(1.0 - a1, 1.0, 1.0 - b0, 1.0 - b1, y3) < x3;
            break;
            case 3:
                if (x3 > b1) break;
                out = getInterceptY(0.0, 1.0 - a0, b1, 1.0 - b0, x3) < y3
                    && getInterceptY(0.0, 1.0 - a1, b1, 1.0 - b0, x3) > y3;
            break;
        }
        if (out) break;
    }
    return !out;
}

bool inRangeY(const Line& line, const float& y3) {
    std::vector<std::pair<float, float>> arr {
        std::pair<float, float> {line.ax, line.ay},
        std::pair<float, float> {line.bx, line.by}
    };
    float min, max;
    minmaxY(arr, min, max);
    return min <= y3 && y3 <= max;
}

int findNeighbors(const int& x, const int& y, const Tilemap& tiles) {
    int n = 0, area = tiles.at(x, y).area;
    for (int y0 = y - 1; y0 < y + 2; y0++) {
        for (int x0 = x - 1; x0 < x + 2; x0++) {
            if (x0 < 0 || y0 < 0
                || x0 >= tiles.totalx || y0 >= tiles.totaly
                || (x0 == x && y0 == y)
                || tiles.at(x, y).area != area
            ) continue;
            n++;
        }
    }
    return n;
}

int distanceFromUnlike(const int& x, const int& y, const Tilemap& tiles, const int& lim) {
    int out = -1, area = tiles.at(x, y).area,
        minx, plux, miny, pluy;
    for (int i = 1; i <= lim; i++) {
        minx = (x - i >= 0) ? x - i : 0;
        plux = (x + i < tiles.totalx) ? x + i : tiles.totalx - 1;
        miny = (y - i >= 0) ? y - i : 0;
        pluy = (y + i < tiles.totaly) ? y + i : tiles.totaly - 1;
        if (
            tiles.at(minx, miny).area != area ||
            tiles.at(x, miny).area != area ||
            tiles.at(plux, miny).area != area ||
            tiles.at(minx, y).area != area ||
            tiles.at(plux, y).area != area ||
            tiles.at(minx, pluy).area != area ||
            tiles.at(x, pluy).area != area ||
            tiles.at(plux, pluy).area != area
        ) {
            out = i;
            break;
        }
    }
    return out;
}

int checkUnlikeNeighbor(const int& x, const int& y, const Tilemap& tiles) {
    int out = -1, area = tiles.tilegrid[y][x].area, highest = 0;
    int count[5] {0, 0, 0, 0, 0};
    for (int y0 = (y - 1); y0 < (y + 2); y0++) {
        for (int x0 = (x - 1); x0 < (x + 2); x0++) {
            if (y0 < 0 || x0 < 0 ||
                y0 >= tiles.totaly || x0 >= tiles.totalx ||
                (x == x0 && y == y0) ||
                tiles.tilegrid[y0][x0].area == area
            ) continue;
            count[tiles.tilegrid[y0][x0].area - 1]++;
        }
    }
    for (int i = 0; i < 5; i++) {
        if (i + 1 != area && count[i] > 0 && count[i] > highest) {
            highest = count[i];
            out = i;
        }
    }
    return out + 1;
}

// Debug function
void outputWorld(const Tilemap& tiles) {
    int code;
    std::string colors[6] {
        "\x1b[00m",
        "\x1b[31m",
        "\x1b[36m",
        "\x1b[33m",
        "\x1b[34m",
        "\x1b[35m"
    };
    for (int y = 0; y < tiles.totaly; y++) {
        for (int x = 0; x < tiles.totalx; x++) {
            code = tiles.tilegrid[y][x].tilecode();
            std::cout << colors[code] << std::to_string(code);
        }
        std::cout << std::endl;
    }
    std::cout << "\x1b[00m";
}