#include "../include/worldgen/WorldData.h"

Tiletype::Tiletype(TileClass _t, int _code, std::vector<int> _colors, int _weight)
    : tileclass(_t), code(_code), colors(_colors), weight(_weight) {}

Tiletype::Tiletype(){}

std::string loadFile(std::string path) {
    std::string line;
    std::string output;
    std::fstream file(path.c_str(), std::fstream::in);
    if (file.is_open()) {
        while(std::getline(file, line))
            output += line;
    }
    file.close();
    return output;
}

rapidjson::Document parseJson(std::string json) {
    rapidjson::Document document;
    rapidjson::ParseResult valid = document.Parse(json.c_str());
    if (!valid) {
        std::cerr << "Unable to Parse JSON: Code " << valid.Code() << " at position (" << valid.Offset() << ")"
            << std::endl;
        std::cerr << "Snippet after position:" << std::endl;
        std::cerr << json.substr(valid.Offset() - 1) << std::endl;
    }
    return document;
}

rapidjson::Document loadTiles() {
    std::string json = loadFile("./data/tiles.json");
    return parseJson(json);
}

rapidjson::Document loadColors() {
    std::string json = loadFile("./data/colors.json");
    return parseJson(json);
}

std::string generateJsonPointer(std::vector<std::string> values) {
    std::string out;
    for (int i = 0; i < values.size(); i++) {
        out += "/" + values[i];
    }
    return out;
}

Tileset Data::getTileset(int temp, int moisture, int tier, float terrain, float foliage) {
    std::cout << "Building Tileset" << std::endl;
    rapidjson::Document tiledata = loadTiles();
    rapidjson::Document colordata = loadColors();
    rapidjson::Value* pointer;

    Tileset tileset;
    Tiletype t;
    std::vector<int> colorkeys;
    std::map<int, std::string> colors;

    int flag, c;
    int conditionals[] = {temp, moisture, tier};
    std::string tags[] = {"temp", "moisture", "tier"};

    for (int i = 0; i < 5; i++) {
        tileset.weightmap.push_back(std::vector<Tiletype*>{});

        // For each TileClass, iterate through options
        for (auto& tiletype : tiledata[i].GetObject()) {
            pointer = rapidjson::Pointer(generateJsonPointer(std::vector<std::string>{
                std::to_string(i),
                tiletype.name.GetString(),
                "req"
            }).c_str()).Get(tiledata);

            // Check if this tiletype can be used in this context
            flag = 0;
            for (int x = 0; x < 3; x++) {
                for (auto& value : pointer->GetObject()[(&tags[x])->c_str()].GetArray()) {
                    if (value.GetInt() == -255 || value.GetInt() == conditionals[x]) {
                        flag++;
                        break;
                    }
                }
            }

            // TODO - check for other conditionals
            if (flag == 3) {
                // Add colors to tileset
                colorkeys.clear();
                for (auto& color : pointer->GetObject()["colors"].GetArray()) {
                    c = color.GetInt();
                    colorkeys.push_back(c);
                    if (colors.find(c) != colors.end()) {
                        colors.insert(std::pair<int, std::string>{
                            c,
                            colordata[c].GetString()
                        });
                    }
                }

                // Add tiletype to tileset
                t = Tiletype(
                    TileClass(i),
                    std::stoi(tiletype.name.GetString()),
                    colorkeys,
                    pointer->GetObject()["weight"].GetInt()
                );
                tileset.sets[i]->push_back(t);

                // Update weightmap
                for (int y = 0; y < t.weight; y++)
                    tileset.weightmap[i].push_back(&t);
            }
        }
    }
    tileset.colors = colors;

    std::cout << "Tileset built." << std::endl;
    return tileset;
}